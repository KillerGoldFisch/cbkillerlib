package net.gliewe.cb.killerlib;

import org.bukkit.plugin.java.JavaPlugin;

public class KillerLib extends JavaPlugin {

    private static KillerLib _instance = null;

    public static KillerLib getInstance() {
        return _instance;
    }


    @Override
    public void onEnable(){
        this.saveDefaultConfig();

        _instance = this;
    }

    @Override
    public void onDisable() {
        _instance = null;
    }
}